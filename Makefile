all: gvarianttransform

gvarianttransform: gvarianttransform.c Makefile
	$(CC) -o $@ -Wall -ggdb gvarianttransform.c $(shell pkg-config --cflags --libs gobject-2.0)

clean:
	rm -f gvarianttransform
