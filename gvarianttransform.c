/* varianttransforms.c
 *
 * Copyright 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib-object.h>
#include <stdlib.h>

typedef void (*VariantTransform) (GVariant *src,
                                  GValue   *dst);

#define DEFINE_FAST_TRANSFORM(varname, valname, ...)                   \
static void                                                            \
variant_##varname##_to_##valname (GVariant *src,                       \
                                  GValue   *dst)                       \
{                                                                      \
  g_value_set_##valname (dst,                                          \
                         g_variant_get_##varname (src,##__VA_ARGS__)); \
}

DEFINE_FAST_TRANSFORM (boolean, boolean);
DEFINE_FAST_TRANSFORM (string, string, NULL);
DEFINE_FAST_TRANSFORM (double, double);
DEFINE_FAST_TRANSFORM (double, float);
DEFINE_FAST_TRANSFORM (int16, int);
DEFINE_FAST_TRANSFORM (int16, uint);
DEFINE_FAST_TRANSFORM (int16, int64);
DEFINE_FAST_TRANSFORM (int16, uint64);
DEFINE_FAST_TRANSFORM (uint16, int);
DEFINE_FAST_TRANSFORM (uint16, uint);
DEFINE_FAST_TRANSFORM (uint16, int64);
DEFINE_FAST_TRANSFORM (uint16, uint64);
DEFINE_FAST_TRANSFORM (int32, int);
DEFINE_FAST_TRANSFORM (int32, uint);
DEFINE_FAST_TRANSFORM (int32, int64);
DEFINE_FAST_TRANSFORM (int32, uint64);
DEFINE_FAST_TRANSFORM (uint32, int);
DEFINE_FAST_TRANSFORM (uint32, uint);
DEFINE_FAST_TRANSFORM (uint32, int64);
DEFINE_FAST_TRANSFORM (uint32, uint64);
DEFINE_FAST_TRANSFORM (int64, int);
DEFINE_FAST_TRANSFORM (int64, uint);
DEFINE_FAST_TRANSFORM (int64, int64);
DEFINE_FAST_TRANSFORM (int64, uint64);
DEFINE_FAST_TRANSFORM (uint64, int);
DEFINE_FAST_TRANSFORM (uint64, uint);
DEFINE_FAST_TRANSFORM (uint64, int64);
DEFINE_FAST_TRANSFORM (uint64, uint64);

#undef DEFINE_FAST_TRANSFORM

typedef struct {
  VariantTransform transform;
  gunichar         variant_type;
  guint            fund_type;
} VariantDispatch;

static VariantDispatch dispatch_table[] = {
  /* Table must be sorted by vtype then fund_type */
  { variant_boolean_to_boolean, 'b', G_TYPE_BOOLEAN /* 5  */ },

  { variant_double_to_float,    'd', G_TYPE_FLOAT   /* 14 */ },
  { variant_double_to_double,   'd', G_TYPE_DOUBLE  /* 15 */ },

  { variant_int32_to_int,       'i', G_TYPE_INT     /* 6  */ },
  { variant_int32_to_uint,      'i', G_TYPE_UINT    /* 7  */ },
  { variant_int32_to_int64,     'i', G_TYPE_INT64   /* 10 */ },
  { variant_int32_to_uint64,    'i', G_TYPE_UINT64  /* 11 */ },

  { variant_int16_to_int,       'n', G_TYPE_INT     /* 6  */ },
  { variant_int16_to_uint,      'n', G_TYPE_UINT    /* 7  */ },
  { variant_int16_to_int64,     'n', G_TYPE_INT64   /* 10 */ },
  { variant_int16_to_uint64,    'n', G_TYPE_UINT64  /* 11 */ },

  { variant_uint16_to_int,      'q', G_TYPE_INT     /* 6  */ },
  { variant_uint16_to_uint,     'q', G_TYPE_UINT    /* 7  */ },
  { variant_uint16_to_int64,    'q', G_TYPE_INT64   /* 10 */ },
  { variant_uint16_to_uint64,   'q', G_TYPE_UINT64  /* 11 */ },

  { variant_string_to_string,   's', G_TYPE_STRING  /* 16 */ },

  { variant_uint64_to_int,      't', G_TYPE_INT     /* 6  */ },
  { variant_uint64_to_uint,     't', G_TYPE_UINT    /* 7  */ },
  { variant_uint64_to_int64,    't', G_TYPE_INT64   /* 10 */ },
  { variant_uint64_to_uint64,   't', G_TYPE_UINT64  /* 11 */ },

  { variant_uint32_to_int,      'u', G_TYPE_INT     /* 6  */ },
  { variant_uint32_to_uint,     'u', G_TYPE_UINT    /* 7  */ },
  { variant_uint32_to_int64,    'u', G_TYPE_INT64   /* 10 */ },
  { variant_uint32_to_uint64,   'u', G_TYPE_UINT64  /* 11 */ },

  { variant_int64_to_int,       'x', G_TYPE_INT     /* 6  */ },
  { variant_int64_to_uint,      'x', G_TYPE_UINT    /* 7  */ },
  { variant_int64_to_int64,     'x', G_TYPE_INT64   /* 10 */ },
  { variant_int64_to_uint64,    'x', G_TYPE_UINT64  /* 11 */ },
};

static gint
variant_dispatch_compare (gconstpointer a,
                          gconstpointer b)
{
  const VariantDispatch *va = a;
  const VariantDispatch *vb = b;
  gint ret;

  ret = (gint)va->variant_type - (gint)vb->variant_type;
  if (ret != 0)
    return ret;

  return (gint)va->fund_type - (gint)vb->fund_type;
}

static inline const VariantDispatch *
find_dispatch (gchar  variant_type,
               guint8 fundamental_type)
{
  const VariantDispatch key = { NULL, variant_type, fundamental_type };

  return bsearch (&key,
                  dispatch_table,
                  G_N_ELEMENTS (dispatch_table),
                  sizeof (VariantDispatch),
                  variant_dispatch_compare);
}

static void
variant_to_any (const GValue *src,
                GValue       *dst)
{
  GType type = G_VALUE_TYPE (dst);
  const VariantDispatch *dispatch;
  const gchar *typestr;
  GVariant *v;

  /* Ignore complex glib types */
  if (type > G_TYPE_FUNDAMENTAL_MAX)
    {
      g_print ("Womp3\n");
      return;
    }

  if (!(v = g_value_get_variant (src)))
    {
      g_print ("Womp2\n");
      return;
    }

  typestr = (const gchar *)g_variant_get_type_string (v);

  /* We can only do basic types */
  if (typestr[0] == 0 || typestr[1] != 0)
    {
      g_print ("Womp1\n");
      return;
    }

  /* Find our transform for the left/right */
  if ((dispatch = find_dispatch (*typestr, type)))
    {
      dispatch->transform (v, dst);
      return;
    }

  /* Slow path */

  /* String array boxed type */
  if (g_variant_is_of_type (v, G_VARIANT_TYPE_STRING_ARRAY) &&
      G_VALUE_HOLDS (dst, G_TYPE_STRV))
    {
      const gchar **strv = g_variant_get_strv (v, NULL);
      g_value_set_boxed (dst, strv);
      return;
    }
}

void
devd_register_variant_transforms (void)
{
  static gsize initialized;

  if (g_once_init_enter (&initialized))
    {
      for (guint i = 0; i < G_N_ELEMENTS (dispatch_table); i++)
        {
          const VariantDispatch *item = &dispatch_table[i];

          g_value_register_transform_func (G_TYPE_VARIANT,
                                           item->fund_type,
                                           variant_to_any);
        }
      g_once_init_leave (&initialized, TRUE);
    }
}

#define TRANSFORM_TEST(vname, vval, VTYPE, vget, cmp) \
  G_STMT_START { \
    GVariant *var = g_variant_new_##vname (vval); \
    GValue val = G_VALUE_INIT; \
    GValue val2 = G_VALUE_INIT; \
    g_value_init (&val, G_TYPE_VARIANT); \
    g_value_set_variant (&val, var); \
    g_value_init (&val2, G_TYPE_##VTYPE); \
    g_value_transform (&val, &val2); \
    g_assert_##cmp (g_value_get_##vget (&val2), ==, vval); \
  } G_STMT_END

gint
main (gint   argc,
      gchar *argv[])
{
  devd_register_variant_transforms ();

  TRANSFORM_TEST (int16, 123, INT, int, cmpint);
  TRANSFORM_TEST (uint16, 123, INT, int, cmpint);
  TRANSFORM_TEST (int32, 123, INT, int, cmpint);
  TRANSFORM_TEST (int64, 123, INT, int, cmpint);
  TRANSFORM_TEST (uint32, 321, INT, int, cmpint);
  TRANSFORM_TEST (uint64, 321, INT, int, cmpint);

  TRANSFORM_TEST (int16, 123, UINT, uint, cmpint);
  TRANSFORM_TEST (uint16, 123, UINT, uint, cmpint);
  TRANSFORM_TEST (int32, 123, UINT, uint, cmpint);
  TRANSFORM_TEST (int64, 123, UINT, uint, cmpint);
  TRANSFORM_TEST (uint32, 321, UINT, uint, cmpint);
  TRANSFORM_TEST (uint64, 321, UINT, uint, cmpint);

  TRANSFORM_TEST (int16, 778, INT64, int64, cmpint);
  TRANSFORM_TEST (uint16, 778, INT64, int64, cmpint);
  TRANSFORM_TEST (int32, 778, INT64, int64, cmpint);
  TRANSFORM_TEST (int64, G_MAXINT64, INT64, int64, cmpint);
  TRANSFORM_TEST (uint32, 778, INT64, int64, cmpint);
  TRANSFORM_TEST (uint64, G_MAXINT64, INT64, int64, cmpint);

  TRANSFORM_TEST (int16, 778, UINT64, uint64, cmpint);
  TRANSFORM_TEST (uint16, 778, UINT64, uint64, cmpint);
  TRANSFORM_TEST (int32, 778, UINT64, uint64, cmpint);
  TRANSFORM_TEST (int64, G_MAXINT64, UINT64, uint64, cmpint);
  TRANSFORM_TEST (uint32, 778, UINT64, uint64, cmpint);
  TRANSFORM_TEST (uint64, G_MAXUINT64, UINT64, uint64, cmpint);

  TRANSFORM_TEST (double, 123.45, DOUBLE, double, cmpint);
  TRANSFORM_TEST (double, 123.45, FLOAT, float, cmpint);

  TRANSFORM_TEST (boolean, TRUE, BOOLEAN, boolean, cmpint);
  TRANSFORM_TEST (boolean, FALSE, BOOLEAN, boolean, cmpint);

  TRANSFORM_TEST (string, "abc", STRING, string, cmpstr);

  return 0;
}
